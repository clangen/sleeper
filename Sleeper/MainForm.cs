﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Threading;

namespace Sleeper
{
    public partial class MainForm : Form
    {
        private NotifyIcon trayIcon;
        private ContextMenu trayMenu;
        private MenuItem enableMenuItem;

        public MainForm()
        {
            InitializeComponent();

            if (SystemInformation.BootMode != BootMode.Normal)
            {
                this.Text = this.Text + " - DISABLED IN SAFE MODE";
            }
        }

        private void InitializeTrayIcon()
        {
            trayMenu = new ContextMenu();

            enableMenuItem = trayMenu.MenuItems.Add("&enabled", OnTrayEnable);
            enableMenuItem.Checked = true;
            trayMenu.MenuItems.Add("&configure", OnTrayConfigure);
            trayMenu.MenuItems.Add("-");
            trayMenu.MenuItems.Add("e&xit", OnTrayExit);

            trayIcon = new NotifyIcon();
            trayIcon.Text = "sleeper";
            trayIcon.Icon = new Icon(SystemIcons.Application, 40, 40);
            trayIcon.Icon = this.Icon;
            trayIcon.ContextMenu = trayMenu;
            trayIcon.Visible = true;
            trayIcon.Click += OnTrayLeftClick;
        }

        private void sendToTray()
        {
            /* we need to have the window be WS_VISIBLE to receive our
            suspension broadcasts. just move it WAY of screen and hide
            it from the task bar */
            this.Left = -32000;
            this.Top = -32000;
            this.ShowInTaskbar = false;
        }

        private void restoreFromTray()
        {
            this.BringToFront();
            this.CenterToScreen();
            this.ShowInTaskbar = true;
        }

        private void OnTrayExit(object sender, EventArgs e)
        {
            Application.Exit();
        }

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == PowerManager.WM_POWERBROADCAST)
            {
                powerEventControl1.OnExternalPowerBroadcast(ref m);
            }
            
            base.WndProc(ref m);
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            InitializeTrayIcon();

            if (!Runtime.InDebugger())
            {
                sendToTray();
            }
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            if (Runtime.InDebugger())
            {
                base.OnClosing(e);
            }
            else
            {
                e.Cancel = true;
                sendToTray();
            }
        }

        private void OnTrayConfigure(object sender, EventArgs e)
        {
            if (e is MouseEventArgs && ((MouseEventArgs) e).Button == MouseButtons.Left)
            {
                restoreFromTray();
            }
        }

        private void OnTrayLeftClick(object sender, EventArgs e)
        {
            restoreFromTray();
        }

        private void OnTrayEnable(object sender, EventArgs e)
        {
            /* ugh, to prevent cascading events, just check the box manually
            and let monitoringEnabled event handler set the rest of the UI */
            monitoringEnabled.Checked = !enableMenuItem.Checked;
        }

        private void checkBox_CheckedChanged(object sender, EventArgs e)
        {
            powerEventControl1.ForceSuspend = forceCheckBox.Checked;
            powerEventControl1.DisableWake = timedWakesCheckBox.Checked;
        }

        private void monitoringEnabled_CheckedChanged(object sender, EventArgs e)
        {
            setEnabled(monitoringEnabled.Checked);
        }

        private void setEnabled(bool enabled)
        {
            monitoringEnabled.Checked = enabled;
            forceCheckBox.Enabled = enabled;
            timedWakesCheckBox.Enabled = enabled;
            powerEventControl1.SuspendEnabled = enabled;
            enableMenuItem.Checked = enabled;
        }

        private void backgroundBtn_Click(object sender, EventArgs e)
        {
            sendToTray();
        }

        private void quitBtn_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
