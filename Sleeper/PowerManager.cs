﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace Sleeper
{
    class PowerManager
    {
        public static Guid GUID_MONITOR_POWER_ON = new Guid("02731015-4510-4526-99e6-e5a17ebd1aea");
        public static Guid GUID_SYSTEM_AWAYMODE = new Guid("98a7f580-01f7-48aa-9c0f-44352c29e5C0");
        public const int WM_POWERBROADCAST = 0x0218;
        public const int DEVICE_NOTIFY_WINDOW_HANDLE = 0x00000000;
        public const int PBT_POWERSETTINGCHANGE = 0x8013;
        public const int PBT_APMRESUMESUSPEND = 0x7;

        [DllImport(@"User32", SetLastError = true,
          EntryPoint = "RegisterPowerSettingNotification",
          CallingConvention = CallingConvention.StdCall)]

        public static extern IntPtr RegisterPowerSettingNotification(
            IntPtr hRecipient,
            ref Guid PowerSettingGuid,
            Int32 Flags);

        [StructLayout(LayoutKind.Sequential, Pack = 4)]
        public struct POWERBROADCAST_SETTING
        {
            public Guid PowerSetting;
            public uint DataLength;
            public byte Data;
        }
    }
}
