﻿namespace Sleeper
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
                trayIcon.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.forceCheckBox = new System.Windows.Forms.CheckBox();
            this.timedWakesCheckBox = new System.Windows.Forms.CheckBox();
            this.delayUpDown = new System.Windows.Forms.NumericUpDown();
            this.monitoringEnabled = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.quitBtn = new System.Windows.Forms.Button();
            this.backgroundBtn = new System.Windows.Forms.Button();
            this.powerEventControl1 = new Sleeper.PowerEventControl();
            ((System.ComponentModel.ISupportInitialize)(this.delayUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(36, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(183, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "suspend after screen has been off for";
            // 
            // forceCheckBox
            // 
            this.forceCheckBox.AutoSize = true;
            this.forceCheckBox.Checked = true;
            this.forceCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.forceCheckBox.Location = new System.Drawing.Point(39, 56);
            this.forceCheckBox.Name = "forceCheckBox";
            this.forceCheckBox.Size = new System.Drawing.Size(60, 17);
            this.forceCheckBox.TabIndex = 3;
            this.forceCheckBox.Text = "\"force\"";
            this.forceCheckBox.UseVisualStyleBackColor = true;
            this.forceCheckBox.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // timedWakesCheckBox
            // 
            this.timedWakesCheckBox.AutoSize = true;
            this.timedWakesCheckBox.Checked = true;
            this.timedWakesCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.timedWakesCheckBox.Location = new System.Drawing.Point(39, 80);
            this.timedWakesCheckBox.Name = "timedWakesCheckBox";
            this.timedWakesCheckBox.Size = new System.Drawing.Size(121, 17);
            this.timedWakesCheckBox.TabIndex = 4;
            this.timedWakesCheckBox.Text = "disable timed wakes";
            this.timedWakesCheckBox.UseVisualStyleBackColor = true;
            this.timedWakesCheckBox.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // delayUpDown
            // 
            this.delayUpDown.Enabled = false;
            this.delayUpDown.Location = new System.Drawing.Point(222, 31);
            this.delayUpDown.Maximum = new decimal(new int[] {
            7200,
            0,
            0,
            0});
            this.delayUpDown.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.delayUpDown.Name = "delayUpDown";
            this.delayUpDown.Size = new System.Drawing.Size(50, 20);
            this.delayUpDown.TabIndex = 5;
            this.delayUpDown.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            // 
            // monitoringEnabled
            // 
            this.monitoringEnabled.AutoSize = true;
            this.monitoringEnabled.Checked = true;
            this.monitoringEnabled.CheckState = System.Windows.Forms.CheckState.Checked;
            this.monitoringEnabled.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.monitoringEnabled.Location = new System.Drawing.Point(12, 12);
            this.monitoringEnabled.Name = "monitoringEnabled";
            this.monitoringEnabled.Size = new System.Drawing.Size(107, 17);
            this.monitoringEnabled.TabIndex = 8;
            this.monitoringEnabled.Text = "global on / off";
            this.monitoringEnabled.UseVisualStyleBackColor = true;
            this.monitoringEnabled.CheckedChanged += new System.EventHandler(this.monitoringEnabled_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(275, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "seconds";
            // 
            // quitBtn
            // 
            this.quitBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.quitBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.quitBtn.Location = new System.Drawing.Point(551, 8);
            this.quitBtn.Name = "quitBtn";
            this.quitBtn.Size = new System.Drawing.Size(84, 28);
            this.quitBtn.TabIndex = 10;
            this.quitBtn.Text = "quit";
            this.quitBtn.UseVisualStyleBackColor = true;
            this.quitBtn.Click += new System.EventHandler(this.quitBtn_Click);
            // 
            // backgroundBtn
            // 
            this.backgroundBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.backgroundBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.backgroundBtn.Location = new System.Drawing.Point(429, 8);
            this.backgroundBtn.Name = "backgroundBtn";
            this.backgroundBtn.Size = new System.Drawing.Size(116, 28);
            this.backgroundBtn.TabIndex = 11;
            this.backgroundBtn.Text = "background";
            this.backgroundBtn.UseVisualStyleBackColor = true;
            this.backgroundBtn.Click += new System.EventHandler(this.backgroundBtn_Click);
            // 
            // powerEventControl1
            // 
            this.powerEventControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.powerEventControl1.DisableWake = true;
            this.powerEventControl1.ForceSuspend = true;
            this.powerEventControl1.Location = new System.Drawing.Point(12, 103);
            this.powerEventControl1.Name = "powerEventControl1";
            this.powerEventControl1.Size = new System.Drawing.Size(619, 261);
            this.powerEventControl1.SuspendEnabled = true;
            this.powerEventControl1.TabIndex = 7;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(643, 377);
            this.ControlBox = false;
            this.Controls.Add(this.backgroundBtn);
            this.Controls.Add(this.quitBtn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.monitoringEnabled);
            this.Controls.Add(this.powerEventControl1);
            this.Controls.Add(this.delayUpDown);
            this.Controls.Add(this.timedWakesCheckBox);
            this.Controls.Add(this.forceCheckBox);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "sleeper";
            ((System.ComponentModel.ISupportInitialize)(this.delayUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox forceCheckBox;
        private System.Windows.Forms.CheckBox timedWakesCheckBox;
        private System.Windows.Forms.NumericUpDown delayUpDown;
        private PowerEventControl powerEventControl1;
        private System.Windows.Forms.CheckBox monitoringEnabled;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button quitBtn;
        private System.Windows.Forms.Button backgroundBtn;
    }
}

