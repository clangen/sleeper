﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sleeper
{
    class Settings
    {
        public int delay { get; set; }
        public bool force { get; set; }
        public bool timers { get; set; }
    }
}
