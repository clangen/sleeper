﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace Sleeper
{
    public partial class PowerEventControl : UserControl
    {
        private const int kDefaultTimeoutMillis = 30000;

        private bool monitorsOff = false;
        private bool force = true, disableWake = true, suspendEnabled = true;
        private System.Timers.Timer timer;
        private int timeoutMillis = kDefaultTimeoutMillis;
        private bool safeMode = (SystemInformation.BootMode != BootMode.Normal);

        public PowerEventControl()
        {
            InitializeComponent();

            PowerManager.RegisterPowerSettingNotification(
                this.Handle,
                ref PowerManager.GUID_MONITOR_POWER_ON,
                PowerManager.DEVICE_NOTIFY_WINDOW_HANDLE);
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            simulateOff.Visible = simulateOn.Visible = Runtime.IsDebug();
        }

        public void OnExternalPowerBroadcast(ref Message m)
        {
            HandlePowerBroadcast(ref m);
        }

        public bool SuspendEnabled
        {
            get
            {
                return suspendEnabled;
            }
            
            set
            {
                suspendEnabled = value;
                debug("setting enabled = " + suspendEnabled);
            }
        }

        public bool ForceSuspend
        {
            get { return force; }
            set { force = value; }
        }

        public bool DisableWake
        {
            get { return disableWake; }
            set { disableWake = value; }
        }

        protected override void WndProc(ref Message m)
        {
            HandlePowerBroadcast(ref m);
            base.WndProc(ref m);
        }

        private void HandlePowerBroadcast(ref Message m)
        {
            if (m.Msg == PowerManager.WM_POWERBROADCAST)
            {
                if (safeMode)
                {
                    debug("received WM_POWERBROADCAST but in safe mode!");
                    return;
                }

                if (!suspendEnabled)
                {
                    debug("received WM_POWERBROADCAST but not enabled.");
                    return;
                }

                if (m.WParam.ToInt32() == PowerManager.PBT_POWERSETTINGCHANGE)
                {
                    PowerManager.POWERBROADCAST_SETTING data = (PowerManager.POWERBROADCAST_SETTING)
                        Marshal.PtrToStructure(m.LParam, typeof(PowerManager.POWERBROADCAST_SETTING));

                    if (data.PowerSetting == PowerManager.GUID_MONITOR_POWER_ON)
                    {
                        setMonitorsOff(data.Data == 0);
                    }
                }
                else if (m.WParam.ToInt32() == PowerManager.PBT_APMRESUMESUSPEND)
                {
                    if (monitorsOff)
                    {
                        debug("detected resume but monitors still off! scheduling suspend...");
                        startTimer();
                    }
                }
            }
        }

        private void setMonitorsOff(bool off)
        {
            debug("monitors " + (off ? "OFF" : "ON"));
            monitorsOff = off;

            lock (this)
            {
                stopTimer();

                if (off)
                {
                    startTimer();
                }
            }
        }

        private void stopTimer()
        {
            if (timer != null)
            {
                timer.Stop();
                timer.Close();
                timer = null;
                debug("stopped timer, suspend CANCELLED");
            }
        }

        private void startTimer()
        {
            if (timer == null)
            {
                timer = new System.Timers.Timer(timeoutMillis);
                timer.Elapsed += OnTimerTick;
                timer.Start();
                debug("started timer, will suspend shortly");
            }
        }

        private void OnTimerTick(object sender, System.Timers.ElapsedEventArgs e)
        {
            lock (this)
            {
                stopTimer();
            }

            Invoke(new MethodInvoker(() =>
            {
                if (monitorsOff)
                {
                    if (safeMode)
                    {
                        debug("normally we'd suspend, but we're in safe mode! bailing");
                    }
                    else if (!suspendEnabled)
                    {
                        debug("not suspending because NOT ENABLED.");
                        return;
                    }
                    else
                    {
                        debug("suspending now! force=" + force + " disableWake=" + disableWake);
                        Application.SetSuspendState(PowerState.Suspend, force, disableWake);
                    }
                }
            }));
        }

        private void debug(string s)
        {
            Invoke(new MethodInvoker(() =>
            {
                if (textBox1.Text.Length > 2097152)
                {
                    textBox1.Text = "*** auto log reset ***";
                }

                textBox1.Text += DateTime.Now.ToString() + " " + s + "\r\n";

                /* scroll to bottom */
                textBox1.SelectionStart = textBox1.TextLength; 
                textBox1.ScrollToCaret();
            }));
        }

        private void simulateOn_Click(object sender, EventArgs e)
        {
            setMonitorsOff(false);
        }

        private void simulateOff_Click(object sender, EventArgs e)
        {
            setMonitorsOff(true);
        }
    }
}
