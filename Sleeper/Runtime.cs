﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Diagnostics;

namespace Sleeper
{
    class Runtime
    {
        public static Boolean InDebugger()
        {
            return System.Diagnostics.Debugger.IsAttached;
        }

        public static bool IsDebug()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            object[] attributes = assembly.GetCustomAttributes(typeof(DebuggableAttribute), true);
            if (attributes == null || attributes.Length == 0)
            {
                return true;
            }

            var d = (DebuggableAttribute) attributes[0];
            return d.IsJITTrackingEnabled;
        }
    }
}
