﻿namespace Sleeper
{
    partial class PowerEventControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.simulateOff = new System.Windows.Forms.Button();
            this.simulateOn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.Location = new System.Drawing.Point(0, 3);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox1.Size = new System.Drawing.Size(555, 182);
            this.textBox1.TabIndex = 0;
            // 
            // simulateOff
            // 
            this.simulateOff.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simulateOff.Location = new System.Drawing.Point(425, 13);
            this.simulateOff.Name = "simulateOff";
            this.simulateOff.Size = new System.Drawing.Size(102, 23);
            this.simulateOff.TabIndex = 10;
            this.simulateOff.Text = "simulate: off";
            this.simulateOff.UseVisualStyleBackColor = true;
            this.simulateOff.Click += new System.EventHandler(this.simulateOff_Click);
            // 
            // simulateOn
            // 
            this.simulateOn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simulateOn.Location = new System.Drawing.Point(317, 13);
            this.simulateOn.Name = "simulateOn";
            this.simulateOn.Size = new System.Drawing.Size(102, 23);
            this.simulateOn.TabIndex = 9;
            this.simulateOn.Text = "simulate: on";
            this.simulateOn.UseVisualStyleBackColor = true;
            this.simulateOn.Click += new System.EventHandler(this.simulateOn_Click);
            // 
            // PowerEventControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.simulateOff);
            this.Controls.Add(this.simulateOn);
            this.Controls.Add(this.textBox1);
            this.Name = "PowerEventControl";
            this.Size = new System.Drawing.Size(558, 188);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button simulateOff;
        private System.Windows.Forms.Button simulateOn;
    }
}
