# sleeper

**sleeper** is a small app for Windows (tested in 8.1) that tries to help users who have problems getting their computer to go to sleep when it should. in my particular case, my monitors enter power-save mode, but the computer doesn't. no amount of **powercfg /requestsoverride** commands, disabling of media sharing, device manager tomfoolery, or disgusting registry tweaks allows my computer to go to sleep reliably. not everyone has this problem, but some of us do.

the app works by keeping track of your monitor's power state. shortly after the monitors enter standby, **sleeper** will make a Windows system call to force the computer to fall asleep. if the computer wakes up spuriously, but the monitors don't, the computer will automatically be put back to sleep after a short delay.

#### waranty
there is **no** warranty. if this app makes your computer explode i'm not responsible. use at your own risk.

#### faq
- **q1**: **sleeper** is freaking out and keeps turning my computer off and i can't stop it!
- **a1**: this shouldn't happen, but if it does: reboot in safe mode and uninstall. **sleeper** has been designed to detect safe mode and disable all functionality
- **q2**: things like media playback no longer keep my computer awake when my monitors enter standby!
- **a2**: this is an unfortunate side-effect of the way **sleeper** works. if you know you're going to be playing media, you can disable **sleeper** by right clicking it in the notification tray.
- **q3**: the code has leaky encapsulations and doesn't follow .NET coding conventions!
- **a3**: that's not really a question, but i know. this app was thrown together in a day after not doing C# development for nearly a decade.

#### license
3-clause bsd
